﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Music : MonoBehaviour {

    protected AudioSource _audioSource;
    protected int songNumber = 0;
    [Header("Audio Clips")]
    [SerializeField]
    public AudioClip[] soundtrack;

    private void Awake()
    {

        _audioSource = GetComponent<AudioSource>();
        Assert.IsNotNull(_audioSource);
    }

    // Use this for initialization
    void Start () {
        Debug.Log("Start: audio" );
        #region
        //if (!_audioSource.playOnAwake)
        //{
        //    songNumber++;
        //    if (songNumber == 8)
        //    {
        //        songNumber = 0;
        //    }

        //    _audioSource.clip = soundtrack[songNumber];
        //    _audioSource.Play();
        //}
        #endregion
        StartAudio();
    }

    void StartAudio()
    {
        if (songNumber >6)
        {
            songNumber = 0;
        }

        _audioSource.clip = soundtrack[songNumber];
        _audioSource.Play();
        _audioSource.loop = true;
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.N))
        {
            songNumber++;
            StartAudio();
        }

        _audioSource.volume = GlobalSettings.MusicVolume;
    }
}
