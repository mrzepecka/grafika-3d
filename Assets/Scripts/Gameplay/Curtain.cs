﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Curtain : MonoBehaviour
{
	[SerializeField] private Cloth _cloth;

	private void Awake()
	{
		Assert.IsNotNull(_cloth);
	}

	private void Start()
	{
		List<CapsuleCollider> capsuleColliders = new List<CapsuleCollider>();
		var players = FindObjectsOfType<Player>();
		foreach (var player in players)
		{
			var playerCapsuleColliders = player.GetComponentsInChildren<CapsuleCollider>();
			foreach (var playerCapsuleCollider in playerCapsuleColliders)
			{
				capsuleColliders.Add(playerCapsuleCollider);
			}
		}

		_cloth.capsuleColliders = capsuleColliders.ToArray();
	}
}