﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
	public static InputType CurrentInputType
	{
		get { return (InputType) PlayerPrefs.GetInt("InputType", 0); }
		set
		{
			PlayerPrefs.SetInt("InputType", (int) value);
			PlayerPrefs.Save();
		}
	}

	private void Start()
	{
		ApplyInputType();
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.X))
		{
			ChangeInputType();
			ApplyInputType();
		}
	}

	private void ApplyInputType()
	{
		var inputs = FindObjectsOfType<MiniSumoInput>();
		foreach (var input in inputs)
		{
			input.ChangeInputType(CurrentInputType);
		}
	}
	
	public void ChangeInputType()
	{
		var currentType = (int)CurrentInputType;
		var nextType = (currentType + 1) % Enum.GetValues(typeof(InputType)).Length;
		CurrentInputType = (InputType) nextType;
		Debug.LogFormat("Input type changed from {0} to {1}", (InputType) currentType, CurrentInputType);
	}
}

public enum InputType
{
	PC,
	PS4
}