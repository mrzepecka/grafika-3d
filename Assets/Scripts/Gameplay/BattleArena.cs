﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Debug = UnityEngine.Debug;

public class BattleArena : MonoBehaviour
{
	public static BattleArena instance;
    private AudioSource _audioSource;
    private int songNumber = 0;
    [Header("Audio Clips")]
    [SerializeField] public AudioClip[] soundtrack;

    [Header("Arena settings")]
	[SerializeField] private int _playersCountToStart = 2;
	[SerializeField] private float _arenaHeightMove = 10f;
	[SerializeField] private float _arenaUpMoveTime = 5f;
	[SerializeField] private float _arenaUpMoveSpeed = 5f;
	[SerializeField] private Collider _idleCollider;
	[SerializeField] private Collider _battleCollider;
	[Header("Unity events")]
	[FormerlySerializedAs("OnArenaStartMovingUp")] [SerializeField] private  UnityEvent _onArenaStartMovingUp;
	[FormerlySerializedAs("OnArenaEndMovingUp")] [SerializeField] private  UnityEvent _onArenaEndMovingUp;
	[FormerlySerializedAs("OnArenaStartMovingDown")] [SerializeField] private  UnityEvent _onArenaStartMovingDown;
	[FormerlySerializedAs("OnArenaEndMovingDown")] [SerializeField] private  UnityEvent _onArenaEndMovingDown;

	private Rigidbody _rigidbody;
	private Vector3 _initPosition;
	private Vector3 _upPosition;
	private bool _isMoving;
    private object cube_script;
    private GameObject themeSong;


    private List<Player> _players = new List<Player>();
	private bool _battleStarted;

	private int PlayersInside
	{
		get { return _players.Count; }
	}
	
	private void Awake()
	{
		instance = this;
        themeSong = GameObject.Find("ThemeMusic");

        _audioSource = GetComponent<AudioSource>();
        Assert.IsNotNull(_audioSource);

        _rigidbody = GetComponent<Rigidbody>();
		Assert.IsNotNull(_rigidbody);

        if (themeSong != null)
        {
           cube_script = themeSong.GetComponent<Music>();
        }
		
		Assert.IsNotNull(_idleCollider);
		Assert.IsNotNull(_battleCollider);
		_idleCollider.enabled = true;
		_battleCollider.enabled = false;
	}

	private void Start()
	{
		_players = new List<Player>();
		_initPosition = transform.position;
		_upPosition = _initPosition + transform.up * _arenaHeightMove;
	}

	private void OnTriggerEnter(Collider other)
	{
		Debug.Log("OnTriggerEnter: " + other.name);
		if (other.CompareTag(Constants.Tags.PLAYER))
		{
			var player = other.GetComponentInParent<Player>();
			if (!ContainsPlayer(player.Id))
			{
				Debug.LogFormat("Player {0} comes on the battle arena.", player.Id);
				_players.Add(player);
				if (PlayersInside >= _playersCountToStart)
				{
					StartBattle();
				}
			}
		}
	}

	private void OnTriggerExit(Collider other)
	{
		Debug.Log("OnTriggerExit: " + other.name);
		if (other.CompareTag(Constants.Tags.PLAYER))
		{
			var player = other.GetComponentInParent<Player>();
			if (ContainsPlayer(player.Id))
			{
				Debug.LogFormat("Player {0} leaves on the battle arena.", player.Id);
				_players.RemoveAt(PlayerIndex(player.Id));
				if (_battleStarted)
				{
					EndBattle();
				}
			}
		}
	}

	private bool ContainsPlayer(int playerId)
	{
		return _players.Where((player, i) => player.Id == playerId).Any();
	}

	private int PlayerIndex(int playerId)
	{
		return _players.IndexOf(_players.Where((player, i) => player.Id == playerId).FirstOrDefault());
	}

	private void StartBattle()
	{
		Debug.LogFormat("Battle started");
		_battleStarted = true;
		_battleCollider.enabled = true;
		_idleCollider.enabled = false;
		StartCoroutine(MoveArenaUpCoroutine());
	}

	private IEnumerator MoveArenaUpCoroutine()
	{
        themeSong.GetComponent<AudioSource>().Pause();
       

        if (!_audioSource.playOnAwake)
        {
            songNumber++;
            if (songNumber == 3)
            {
                songNumber = 0;
            }
          
            _audioSource.clip = soundtrack[songNumber];
            _audioSource.Play();            
            
        }

        yield return new WaitForSeconds(1f);

		while (_isMoving) yield return null;
		
		_onArenaStartMovingUp.Invoke();
		_isMoving = true;

		_rigidbody.isKinematic = false;
		while (transform.position.y < _upPosition.y)
		{
			_rigidbody.velocity = transform.up * _arenaUpMoveSpeed;
			yield return null;
		}
		_rigidbody.velocity = Vector3.zero;
		_rigidbody.isKinematic = true;
		
		_isMoving = false;
		_onArenaEndMovingUp.Invoke();
	}
	
	private void EndBattle()
	{
		Debug.LogFormat("Battle ended");
		_battleStarted = false;
		_idleCollider.enabled = true;
		_battleCollider.enabled = false;
		if (_players.Count == 1)
		{
			Debug.LogFormat("Player {0} wins.", _players[0].Id);
		}

		StartCoroutine(MoveArenaDownCoroutine());
	}
	
	private IEnumerator MoveArenaDownCoroutine()
	{
        _audioSource.Stop();
        themeSong.GetComponent<AudioSource>().Play();
        while (_isMoving) yield return null;
		
		_onArenaStartMovingDown.Invoke();
		_isMoving = true;

		_rigidbody.isKinematic = false;
		while (transform.position.y > _initPosition.y)
		{
			_rigidbody.velocity = transform.up * -_arenaUpMoveSpeed;
			yield return null;
		}
		_rigidbody.velocity = Vector3.zero;
		transform.position = _initPosition;
		_rigidbody.isKinematic = true;
		
		_isMoving = false;
		_onArenaEndMovingDown.Invoke();
	}
}