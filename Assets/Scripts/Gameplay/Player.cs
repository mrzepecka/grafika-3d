﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private int id;

    public int Id
    {
        get { return id; }
    }

    private void Start()
    {
        SetTags(transform, Constants.Tags.PLAYER);
    }

    private void SetTags(Transform target, string tag)
    {
        target.gameObject.tag = tag;
        for (int i = 0; i < target.childCount; i++)
        {
            SetTags(target.GetChild(i), tag);
        }        
    }

    private void OnCollisionEnter(Collision other)
    {
        var collisionPoint = other.contacts[0].point;
        SoundEffectsManager.Instance.PlayEffect_PlayerCollision(other.gameObject.tag, collisionPoint);
    }
}