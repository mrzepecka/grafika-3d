using System;
using UnityEngine;

[RequireComponent(typeof(MiniSumoController))]
public class MiniSumoInput : MonoBehaviour
{
    private const string WIN_OS_PREFIX = "WIN_";
    private const string MAC_OS_PREFIX = "MAC_";
    
    [SerializeField] private InputType _inputType;
    
    private MiniSumoController _miniSumoController;
    private string currentOsPrefix;

    private void Awake()
    {
        _miniSumoController = GetComponent<MiniSumoController>();
    }

    private void Start()
    {
        #if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
        currentOsPrefix = WIN_OS_PREFIX;
        #elif UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
        currentOsPrefix = MAC_OS_PREFIX;
        #endif
    }

    private void FixedUpdate()
    {
        float leftInput = 0f;
        float rightInput = 0f;
        switch (_inputType)
        {
            case InputType.PC_P1:
                leftInput = Input.GetAxis("PC_P1_L_Vertical");
                rightInput = Input.GetAxis("PC_P1_R_Vertical");
                break;
            case InputType.PC_P2:
                leftInput = Input.GetAxis("PC_P2_L_Vertical");
                rightInput = Input.GetAxis("PC_P2_R_Vertical");
                break;
            case InputType.PS4_P1:
                leftInput = Input.GetAxis(currentOsPrefix + "PS4_P1_L_Vertical");
                rightInput = Input.GetAxis(currentOsPrefix + "PS4_P1_R_Vertical");
                break;
            case InputType.PS4_P2:
                leftInput = Input.GetAxis(currentOsPrefix + "PS4_P2_L_Vertical");
                rightInput = Input.GetAxis(currentOsPrefix + "PS4_P2_R_Vertical");
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
        _miniSumoController.Move(leftInput, rightInput);
    }

    public void ChangeInputType(global::InputType inputType)
    {
        var player = GetComponent<Player>();
        _inputType = (InputType) (player.Id + 2 * (int) inputType);
    }

    private enum InputType
    {
        PC_P1,
        PC_P2,
        PS4_P1,
        PS4_P2
    }
}