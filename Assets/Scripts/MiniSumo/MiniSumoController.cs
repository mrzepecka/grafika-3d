using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

public class MiniSumoController : MonoBehaviour
{
    [SerializeField] private WheelCollider[] _leftWheelColliders = new WheelCollider[2];
    [SerializeField] private WheelCollider[] _rightWheelColliders = new WheelCollider[2];
    private WheelCollider[] _allWheelColliders = new WheelCollider[4];
    
    [SerializeField] private GameObject[] _leftWheelMeshes = new GameObject[2];
    [SerializeField] private GameObject[] _rightWheelMeshes = new GameObject[2];
    private GameObject[] _allWheelMeshes = new GameObject[4];

    [SerializeField] private Vector3 _centreOfMassOffset = Vector3.zero;
    [SerializeField] private float _fullTorqueOverAllWheels = 2500f;
    [Range(0, 1)] [SerializeField] private float _tractionControl = 1f; // 0 is no traction control, 1 is full interference
    [SerializeField] private float _brakeTorque = 20000f;
    [SerializeField] private float _reverseTorque = 500f;
    
    [SerializeField] private float _slipLimit = 0.3f;
    
    [SerializeField] private float _rotationMultiplier = 12f;
    [SerializeField] private float _topSpeed = 50f;

    public float TopSpeed
    {
        get { return _topSpeed; }
    }

    private float _currentTorque;
    private Rigidbody _rigidbody;
    
    public float AccelerationInput { get; private set; }
    public float BrakeInput { get; private set; }
    public float CurrentSpeed{ get { return _rigidbody.velocity.magnitude*2.23693629f; }}

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        Assert.IsNotNull(_rigidbody);
    }

    private void Start()
    {
        _allWheelColliders = _leftWheelColliders.Concat(_rightWheelColliders).ToArray();
        _allWheelMeshes = _leftWheelMeshes.Concat(_rightWheelMeshes).ToArray();
        _allWheelColliders[0].attachedRigidbody.centerOfMass = _centreOfMassOffset;

        _currentTorque = _fullTorqueOverAllWheels - (_tractionControl * _fullTorqueOverAllWheels);
    }

    private void Move(float acceleration, float brake, bool right)
    {
        for (var i = 0; i < _allWheelColliders.Length; i++)
        {
            Quaternion rotation;
            Vector3 position;
            
            _allWheelColliders[i].GetWorldPose(out position, out rotation);
            var meshTransform = _allWheelMeshes[i].transform; 
            meshTransform.rotation = rotation;
        }

        // Clamp input values
        AccelerationInput = acceleration = Mathf.Clamp(acceleration, 0, 1);
        BrakeInput = brake = -1 * Mathf.Clamp(brake, -1, 0);

        ApplyDrive(acceleration, brake, right);
        CapSpeed();
        TractionControl();
    }
    
    private void ApplyDrive(float acceleration, float brake, bool right)
    {
        var currentWheelColliders = right ? _rightWheelColliders : _leftWheelColliders;

        var thrustTorque = acceleration * (_currentTorque / 4f);
        
        foreach (var wheelCollider in currentWheelColliders)
        {
            wheelCollider.motorTorque = thrustTorque;
                
            if (CurrentSpeed > 5 && Vector3.Angle(transform.forward, _rigidbody.velocity) < 50f)
            {
                wheelCollider.brakeTorque = _brakeTorque * brake;
            }
            else if (brake > 0)
            {
                wheelCollider.brakeTorque = 0f;
                wheelCollider.motorTorque = -_reverseTorque * brake;
            }
        }
    }
    
    private void CapSpeed()
    {
        var speed = _rigidbody.velocity.magnitude;
        speed *= 3.6f;
        if (speed > _topSpeed)
        {
            _rigidbody.velocity = (_topSpeed/ 3.6f) * _rigidbody.velocity.normalized;
        }
    }
    
    private void TractionControl()
    {
        foreach (var wheelCollider in _allWheelColliders)
        {
            WheelHit wheelHit;
            wheelCollider.GetGroundHit(out wheelHit);
            AdjustTorque(wheelHit.forwardSlip);
        }
    }
    
    private void AdjustTorque(float forwardSlip)
    {
        if (forwardSlip >= _slipLimit && _currentTorque >= 0)
        {
            _currentTorque -= 10 * _tractionControl;
        }
        else
        {
            _currentTorque += 10 * _tractionControl;
            if (_currentTorque > _fullTorqueOverAllWheels)
            {
                _currentTorque = _fullTorqueOverAllWheels;
            }
        }
    }

    private void AdjustRotation(float leftInput, float rightInput)
    {
        var rotation = leftInput - rightInput;
//        if (CurrentSpeed < 5f)
//        {
//            _rotationMultiplier = 15;
//        }
//        else if (CurrentSpeed < 10f)
//        {
//            _rotationMultiplier = 15;
//        }
//        else
//        {
//            _rotationMultiplier = 12;
//        }
        _rigidbody.AddRelativeTorque(transform.up * rotation * _rotationMultiplier, ForceMode.Impulse);
    }

    public void Move(float leftInput, float rightInput)
    {
        leftInput = Mathf.Clamp(leftInput, -1, 1);
        rightInput = Mathf.Clamp(rightInput, -1, 1);
        
        Move(leftInput, leftInput, false);
        Move(rightInput, rightInput, true);
        
        AdjustRotation(leftInput, rightInput);
    }
}