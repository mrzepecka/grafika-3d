﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitch : MonoBehaviour
{

    public Camera[] playerThirdPersonCameras;
    public Camera[] playerFirstPersonCameras;
    public Camera gameCamera;
    
//    public GameObject cameraOne;
//    public GameObject cameraTwo;
//    public GameObject cameraThree;
//
//    AudioListener cameraOneAudioLis;
//    AudioListener cameraTwoAudioLis;
//    AudioListener cameraThreeAudioLis;

    // Use this for initialization
    void Start()
    {

        //Get Camera Listeners
//        cameraOneAudioLis = cameraOne.GetComponent<AudioListener>();
//        cameraTwoAudioLis = cameraTwo.GetComponent<AudioListener>();
//        cameraThreeAudioLis = cameraThree.GetComponent<AudioListener>();

        //Camera Position Set
        cameraPositionChange(PlayerPrefs.GetInt("CameraPosition"));
    }

    // Update is called once per frame
    void Update()
    {
        //Change Camera Keyboard
        switchCamera();
    }

    //UI JoyStick Method
    public void cameraPositonM()
    {
        cameraChangeCounter();
    }

    //Change Camera Keyboard
    void switchCamera()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            cameraChangeCounter();
        }
    }

    //Camera Counter
    void cameraChangeCounter()
    {
        int cameraPositionCounter = PlayerPrefs.GetInt("CameraPosition");
        cameraPositionCounter++;
        Debug.Log("cameraPositionCounter " + cameraPositionCounter);
        cameraPositionChange(cameraPositionCounter);
    }

    //Camera change Logic
    void cameraPositionChange(int camPosition)
    {
        if (camPosition == 1)
        {
            camPosition = 1;
        }
        if (camPosition == 2)
        {
            camPosition = 2;

        }
        else if (camPosition > 2)
        {

            camPosition = 0;
        }
        //Set camera position database
        PlayerPrefs.SetInt("CameraPosition", camPosition);

        //Set camera position 1
        if (camPosition == 0)
        {

            Debug.Log("CAM_0");
            ShowThirdPersonCamera();
            
            /*cameraOne.SetActive(true);
            cameraOneAudioLis.enabled = true;

            cameraTwoAudioLis.enabled = false;
            cameraTwo.SetActive(false);

            cameraThreeAudioLis.enabled = false;
            cameraThree.SetActive(false);*/
        }

        //Set camera position 2
        if (camPosition == 1)
        {
            Debug.Log("CAM_1");
            ShowFirstPersonCamera();
            
            /*cameraOneAudioLis.enabled = false;
            cameraOne.SetActive(false);

            cameraTwo.SetActive(true);
            cameraTwoAudioLis.enabled = true;

            cameraThreeAudioLis.enabled = false;
            cameraThree.SetActive(false);*/
        }

        //Set camera position 3
        if (camPosition == 2)
        {
            Debug.Log("CAM_2");
            ShowGameCamera();
            
            /*cameraOneAudioLis.enabled = false;
            cameraOne.SetActive(false);

            cameraTwo.SetActive(false);
            cameraTwoAudioLis.enabled = false;

            cameraThreeAudioLis.enabled = true;
            cameraThree.SetActive(true);*/
        }

    }

    private void ShowGameCamera()
    {
        gameCamera.gameObject.SetActive(true);
        gameCamera.rect = new Rect(0,0,1,1);
        gameCamera.gameObject.GetComponent<AudioListener>().enabled = true;

        foreach (Camera playerFirstPersonCamera in playerFirstPersonCameras)
        {
            playerFirstPersonCamera.gameObject.SetActive(false);
            playerFirstPersonCamera.gameObject.GetComponent<AudioListener>().enabled = false;
        }

        for (var i = 0; i < playerThirdPersonCameras.Length; i++)
        {
            Camera playerThirdPersonCamera = playerThirdPersonCameras[i];
            playerThirdPersonCamera.gameObject.SetActive(true);
            playerThirdPersonCamera.rect = new Rect(0.05f + i * 0.8f, 0.05f, 0.1f, 0.1f);
            playerThirdPersonCamera.gameObject.GetComponent<AudioListener>().enabled = false;
        }
    }
    
    private void ShowFirstPersonCamera()
    {
        gameCamera.gameObject.SetActive(true);
        gameCamera.rect = new Rect(0.45f, 0.85f, 0.1f, 0.1f);
        gameCamera.gameObject.GetComponent<AudioListener>().enabled = false;

        for (var i = 0; i < playerFirstPersonCameras.Length; i++)
        {
            Camera playerFirstPersonCamera = playerFirstPersonCameras[i];
            playerFirstPersonCamera.gameObject.SetActive(true);
            playerFirstPersonCamera.rect = new Rect(i * 0.5f, 0f, 0.5f, 1f);
            playerFirstPersonCamera.gameObject.GetComponent<AudioListener>().enabled = true;
        }

        foreach (var playerThirdPersonCamera in playerThirdPersonCameras)
        {
            playerThirdPersonCamera.gameObject.SetActive(false);
            playerThirdPersonCamera.gameObject.GetComponent<AudioListener>().enabled = false;
        }
    }
    
    private void ShowThirdPersonCamera()
    {
        gameCamera.gameObject.SetActive(false);
        gameCamera.gameObject.GetComponent<AudioListener>().enabled = false;

        for (var i = 0; i < playerFirstPersonCameras.Length; i++)
        {
            Camera playerFirstPersonCamera = playerFirstPersonCameras[i];
            playerFirstPersonCamera.gameObject.SetActive(true);
            playerFirstPersonCamera.rect = new Rect(0.05f + i * 0.8f, 0.05f, 0.1f, 0.1f);
            playerFirstPersonCamera.gameObject.GetComponent<AudioListener>().enabled = false;
        }

        for (var i = 0; i < playerThirdPersonCameras.Length; i++)
        {
            Camera playerThirdPersonCamera = playerThirdPersonCameras[i];
            playerThirdPersonCamera.gameObject.SetActive(true);            
            playerThirdPersonCamera.rect = new Rect(i * 0.5f, 0f, 0.5f, 1f);
            playerThirdPersonCamera.gameObject.GetComponent<AudioListener>().enabled = true;
        }
    }
}