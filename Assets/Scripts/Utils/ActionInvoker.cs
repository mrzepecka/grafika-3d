﻿using System;

public static class ActionInvoker
{
	public static void InvokeAction(Action action)
	{
		if (action != null)
			action();
	}

	public static void InvokeAction<T>(Action<T> action, T arg)
	{
		if (action != null)
			action(arg);
	}

	public static void InvokeAction<T1, T2>(Action<T1, T2> action, T1 arg1, T2 arg2)
	{
		if (action != null)
			action(arg1, arg2);
	}

	public static void InvokeAction<T1, T2, T3>(Action<T1, T2, T3> action, T1 arg1, T2 arg2, T3 arg3)
	{
		if (action != null)
			action(arg1, arg2, arg3);
	}
}