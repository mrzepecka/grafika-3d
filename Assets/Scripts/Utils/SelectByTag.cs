﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;
 
public class SelectByTag : MonoBehaviour {
 
	private static string SelectedTag = "Metal";
 
	[MenuItem("Helpers/Select By Tag")]
	public static void SelectObjectsWithTag()
	{
		GameObject[] objects = GameObject.FindGameObjectsWithTag(SelectedTag);
		Selection.objects = objects;
	}
	
	[MenuItem("Helpers/Select By Tag Metal")]
	public static void SelectObjectsWithTagMetal()
	{
		GameObject[] objects = GameObject.FindGameObjectsWithTag(Constants.Tags.METAL);
		Selection.objects = objects;
	}
	
	[MenuItem("Helpers/Select By Tag Concrete")]
	public static void SelectObjectsWithTagConcrete()
	{
		GameObject[] objects = GameObject.FindGameObjectsWithTag(Constants.Tags.CONCRETE);
		Selection.objects = objects;
	}
	
	[MenuItem("Helpers/Select By Tag Wood")]
	public static void SelectObjectsWithTagWood()
	{
		GameObject[] objects = GameObject.FindGameObjectsWithTag(Constants.Tags.WOOD);
		Selection.objects = objects;
	}
}
#endif