﻿using System;
using System.Collections;
using UnityEngine;

public static class CanvasGroupAnimator
{
	public static void HideAnimation(CanvasGroup canvasGroup, View view, Action onHideStart = null, Action onHideEnd = null, float hideTime = Constants.DEFAULT_HIDING_TIME)
	{
		view.StartCoroutine(AnimateCoroutine(canvasGroup, 1f, 0f, hideTime, onHideStart, onHideEnd));
	}

	public static void ShowAnimation(CanvasGroup canvasGroup, View view, Action onShowStart = null, Action onShowEnd = null, float showTime = Constants.DEFAULT_SHOWING_TIME)
	{
		view.StartCoroutine(AnimateCoroutine(canvasGroup, 0f, 1f, showTime, onShowStart, onShowEnd));
	}

	public static IEnumerator AnimateCoroutine(CanvasGroup canvasGroup, float startValue, float endValue, float changeTime,
		Action onChangeStart = null, Action onChangeEnd = null)
	{
		SetUpCanvasGroup(canvasGroup, startValue);
		yield return new WaitForEndOfFrame();

		ActionInvoker.InvokeAction(onChangeStart);

		bool countingDown = startValue > endValue;
		float timer = changeTime;
		while (timer > 0)
		{
			float progress = countingDown ? timer / changeTime : (changeTime - timer) / changeTime;
			canvasGroup.alpha = progress;
			timer -= Time.deltaTime;
			yield return null;
		}
		SetUpCanvasGroup(canvasGroup, endValue);
		yield return new WaitForEndOfFrame();

		ActionInvoker.InvokeAction(onChangeEnd);
	}

	public static void SetUpCanvasGroup(CanvasGroup canvasGroup, float value)
	{
		canvasGroup.alpha = value;
		canvasGroup.interactable = value > 0f;
		canvasGroup.blocksRaycasts = value > 0f;
	}
}