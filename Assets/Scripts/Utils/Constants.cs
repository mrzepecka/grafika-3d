﻿public static class Constants
{
	public const float DEFAULT_SHOWING_TIME = 0.5f;
	public const float DEFAULT_HIDING_TIME = 0.5f;

    internal class Tags
    {
        public const string PLAYER = "Player";
        public const string METAL = "Metal";
        public const string CONCRETE = "Concrete";
        public const string CLOTH = "Cloth";
        public const string WOOD = "Wood";
        public const string GROUND = "Ground";
    }
}