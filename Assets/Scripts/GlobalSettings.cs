﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalSettings
{
    public static void ClearSettings()
    {
        PlayerPrefs.DeleteKey("GlobalSettings-MusicVolume");
        PlayerPrefs.DeleteKey("GlobalSettings-SoundEffectsVolume");
        PlayerPrefs.DeleteKey("GlobalSettings-SomeVariable");
        
        PlayerPrefs.Save();
    }
    
    public static float MusicVolume
    {
        get
        {
            return PlayerPrefs.GetFloat("GlobalSettings-MusicVolume", 1.0f);
        }
        set
        {
            PlayerPrefs.SetFloat("GlobalSettings-MusicVolume", value);
            PlayerPrefs.Save();
        }
    }
    
    public static float SoundEffectsVolume
    {
        get
        {
            return PlayerPrefs.GetFloat("GlobalSettings-SoundEffectsVolume", 1.0f);
        }
        set
        {
            PlayerPrefs.SetFloat("GlobalSettings-SoundEffectsVolume", value);
            PlayerPrefs.Save();
        }
    }
    
    public static float SomeVariable
    {
        get
        {
            return PlayerPrefs.GetFloat("GlobalSettings-SomeVariable", 1.0f);
        }
        set
        {
            PlayerPrefs.SetFloat("GlobalSettings-SomeVariable", value);
            PlayerPrefs.Save();
        }
    }
}
