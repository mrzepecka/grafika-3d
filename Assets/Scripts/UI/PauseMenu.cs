﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : View
{
    [SerializeField] private Button _resetButton;
    [SerializeField] private Button _exitButton;
    [SerializeField] private TextMeshProUGUI _tipText;
    [SerializeField] private GameObject _ps4Controller;
    [SerializeField] private GameObject _pcController;
    
    private bool _active;
    private bool _busy;

    private bool _cursorVisibility;
    private CursorLockMode _cursorLockMode;

    protected override void Awake()
    {
        base.Awake();
        
        Assert.IsNotNull(_resetButton);
        Assert.IsNotNull(_exitButton);
        Assert.IsNotNull(_tipText);
        Assert.IsNotNull(_ps4Controller);
        Assert.IsNotNull(_ps4Controller);
    }

    private void Start()
    {
        OnShowing += () =>
        {
            _busy = true;
            _cursorVisibility = Screen.lockCursor;
        };
        OnShown += () =>
        {
            // PAUSE
            _busy = false;
            _active = true;
            _tipText.text = "ESC - POWRÓT";
            Time.timeScale = 0f;

            Screen.lockCursor = false;           
        };
        OnHiding += () =>
        {
            Time.timeScale = 1f;
            Screen.lockCursor = _cursorVisibility;
            _busy = true;
        };
        OnHidden += () =>
        {
            // PLAY
            _busy = false;
            _active = false;
            _tipText.text = "ESC - PAUZA";
            
        };
        
        Hide();
    }

    private void OnEnable()
    {
        _resetButton.onClick.AddListener(ResetButton_OnClick);
        _exitButton.onClick.AddListener(ExitButton_OnClick);
    }

    private void OnDisable()
    {
        _resetButton.onClick.RemoveListener(ResetButton_OnClick);
        _exitButton.onClick.RemoveListener(ExitButton_OnClick);
    }

    private void Update()
    {
        if (_busy)
        {
            return;
        }
        
        _ps4Controller.SetActive(InputManager.CurrentInputType == InputType.PS4);
        _pcController.SetActive(InputManager.CurrentInputType == InputType.PC);
        
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (_active)
            {
                Hide();
            }
            else
            {
                Show();
            }
        }
    }

    private void ResetButton_OnClick()
    {
        if (_busy)
        {
            return;
        }
        
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    
    private void ExitButton_OnClick()
    {
        if (_busy)
        {
            return;
        }

        Time.timeScale = 1f;
        Hide();
        SceneManager.LoadScene("MainMenu");
    }
}