﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class BackToMenuButton : MonoBehaviour
{
	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			OnClick();
		}
	}

	private void OnClick()
	{
		SceneManager.LoadScene(0);
	}
}