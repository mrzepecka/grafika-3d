﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ViewMainMenu : View
{
    [SerializeField] private Button _playButton;
    [SerializeField] private Button _settingsButton;
    [SerializeField] private Button _tutorialButton;
    [SerializeField] private Button _exitButton;

    private void Awake()
    {
        Assert.IsNotNull(_playButton);
        Assert.IsNotNull(_settingsButton);
        Assert.IsNotNull(_tutorialButton);
        Assert.IsNotNull(_exitButton);
    }

    private void OnEnable()
    {
        _playButton.onClick.AddListener(PlayButton_OnClick);
        _settingsButton.onClick.AddListener(SettingsButton_OnClick);
        _tutorialButton.onClick.AddListener(TutorialButton_OnClick);
        _exitButton.onClick.AddListener(ExitButton_OnClick);
    }

    private void OnDisable()
    {
        _playButton.onClick.RemoveListener(PlayButton_OnClick);
        _settingsButton.onClick.RemoveListener(SettingsButton_OnClick);
        _tutorialButton.onClick.RemoveListener(TutorialButton_OnClick);
        _exitButton.onClick.RemoveListener(ExitButton_OnClick);
    }

    private void PlayButton_OnClick()
    {
        SceneManager.LoadScene(1);
    }
    
    private void SettingsButton_OnClick()
    {
        ViewManager.Instance.Show<ViewSettings>();
    }
    
    private void TutorialButton_OnClick()
    {
        ViewManager.Instance.Show<ViewTutorial>();
    }
    
    private void ExitButton_OnClick()
    {
        Application.Quit();
    }
}
