﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class ViewSettings : View
{
    [SerializeField] private Slider _musicSlider;
    [SerializeField] private Slider _sfxSlider;
    [SerializeField] private Slider _someVariableSlider;
    
    [SerializeField] private Button _backButton;

    private void Awake()
    {
        Assert.IsNotNull(_musicSlider);
        Assert.IsNotNull(_sfxSlider);
        Assert.IsNotNull(_someVariableSlider);
        Assert.IsNotNull(_backButton);
    }

    private void Start()
    {
        _musicSlider.value = GlobalSettings.MusicVolume;
        _sfxSlider.value = GlobalSettings.SoundEffectsVolume;
        _someVariableSlider.value = GlobalSettings.SomeVariable;
    }

    private void OnEnable()
    {
        _musicSlider.onValueChanged.AddListener(MusicSlider_OnValueChanged);
        _sfxSlider.onValueChanged.AddListener(SfxSlider_OnValueChanged);
        _someVariableSlider.onValueChanged.AddListener(SomeVariableSlider_OnValueChanged);
        _backButton.onClick.AddListener(BackButton_OnClick);
    }

    private void OnDisable()
    {
        _musicSlider.onValueChanged.RemoveListener(MusicSlider_OnValueChanged);
        _sfxSlider.onValueChanged.RemoveListener(SfxSlider_OnValueChanged);
        _someVariableSlider.onValueChanged.RemoveListener(SomeVariableSlider_OnValueChanged);
        _backButton.onClick.RemoveListener(BackButton_OnClick);
    }

    private void MusicSlider_OnValueChanged(float value)
    {
        GlobalSettings.MusicVolume = value;
    }
    
    private void SfxSlider_OnValueChanged(float value)
    {
        GlobalSettings.SoundEffectsVolume = value;
    }
    
    private void SomeVariableSlider_OnValueChanged(float value)
    {
        GlobalSettings.SomeVariable = value;
    }
    
    private void BackButton_OnClick()
    {
        ViewManager.Instance.BackToPreviousView();
    }
}