﻿using System;
using UnityEngine;
using UnityEngine.Assertions;

public abstract class View : MonoBehaviour
{
    public event Action OnShowing;
    public event Action OnShown;
    public event Action OnHiding;
    public event Action OnHidden;

    [SerializeField] protected CanvasGroup _content;
	[SerializeField] protected float _showingTime = Constants.DEFAULT_SHOWING_TIME;
	[SerializeField] protected float _hidingTime = Constants.DEFAULT_HIDING_TIME;

    protected virtual void Awake()
    {
        Assert.IsNotNull(_content);
    }

    public virtual void Show()
    {
        CanvasGroupAnimator.ShowAnimation(_content, this, OnShowing_Invoke, OnShown_Invoke, _showingTime);
    }

    public virtual void Hide()
    {
        CanvasGroupAnimator.HideAnimation(_content, this, OnHiding_Invoke, OnHidden_Invoke, _hidingTime);
    }

    protected void OnShowing_Invoke()
    {
        ActionInvoker.InvokeAction(OnShowing);
    }

    protected void OnShown_Invoke()
    {
        ActionInvoker.InvokeAction(OnShown);
    }

    protected void OnHiding_Invoke()
    {
        ActionInvoker.InvokeAction(OnHiding);
    }

    protected void OnHidden_Invoke()
    {
        ActionInvoker.InvokeAction(OnHidden);
    }
}