﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class ViewManager : MonoBehaviour
{
    public static ViewManager Instance;
    
    [SerializeField] private List<View> _viewPrefabs;
    private List<View> _views = new List<View>();
    private List<View> _openViews = new List<View>();
    private View _currentView
    {
        get { return _openViews.Count > 0 ? _openViews[_openViews.Count - 1] : null; }
    }

    private void Awake()
    {
        Instance = this;
        Assert.IsNotNull(_viewPrefabs);
    }

    private void Start()
    {
        Show<ViewMainMenu>();
    }

    private T GetViewFromList<T>() where T : View
    {
        T view = null;

        for (int i = 0; i < _views.Count; i++)
        {
            if (_views[i].GetType() == typeof(T))
            {
                view = (T)_views[i];
                break;
            }
        }

        if (view == null)
        {
            view = GetNewView<T>();
        }

        return view;
    }

    private T GetNewView<T>() where T : View
    {
        T view = null;

        for (int i = 0; i < _viewPrefabs.Count; i++)
        {
            if (_viewPrefabs[i].GetType() == typeof(T))
            {
                view = (T)Instantiate(_viewPrefabs[i], transform);
                view.transform.localScale = new Vector3(1, 1, 1);
                _views.Add(view);
                break;
            }
        }

        return view;
    }

    public T GetView<T>() where T: View
    {
        return GetViewFromList<T>();
    }

	public void Show<T>(bool hideCurrentView = true) where T: View
	{
		Debug.Log("[ViewManager] Showing " + typeof(T));
		
		T view = GetViewFromList<T>();
		
		if (view == null)
		{
			Debug.LogError("[ViewManager] There is no view of type " + typeof(T) + "! Can't show this view.");
		}
		else
		{
		    if (hideCurrentView)
		    {
                HideCurrentView();
		    }
			view.Show();
            _openViews.Add(view);
		}
	}
	
	public void Hide<T>() where T : View
	{
        Debug.Log("[ViewManager] Hiding " + typeof(T));

        T view = GetViewFromList<T>();
		
		if (view == null)
		{
			Debug.LogError("[ViewManager] The is no view of type " + typeof(T) + "! Can't hide this view.");
		}
		else
		{
			view.Hide();
		}
	}

    public void HideCurrentView()
    {
        if (_currentView != null)
        {
            Debug.Log("[ViewManager] Hiding " + _currentView);
            _currentView.Hide();
        }
    }

    public bool BackToPreviousView()
    {
        if (_openViews.Count <= 1 )
        {
            Debug.LogWarning("[ViewManager] Can't back to previous view because it not exists.");
            return false;
        }
        _currentView.Hide();
        _openViews.RemoveAt(_openViews.Count - 1);
        _currentView.Show();
        return true;
    }

    public bool BackToView<T>() where T : View
    {
        if (_openViews.Count < 1)
        {
            Debug.LogError("[ViewManager] Can't back to previous view because it not exists.");
            return false;
        }

        int index = -1;
        for (int i = _openViews.Count - 1; i >= 0; i--)
        {
            if (_openViews[i].GetType() == typeof(T))
            {
                index = i;
            }
        }

        if (index == -1)
        {
            Debug.LogErrorFormat("[ViewManager] Can't back to view {0} because it not exists.", typeof(T));
            return false;
        }

        int stepsToBack = (_openViews.Count - 1) - index;
        for (int i = 0; i < stepsToBack; i++)
        {
            BackToPreviousView();
        }

        return true;
    }
}