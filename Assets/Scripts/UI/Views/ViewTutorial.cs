﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class ViewTutorial : View
{

	[SerializeField] private Button _backButton;
	[SerializeField] private GameObject _ps4Controller;
	[SerializeField] private GameObject _pcController;

	private void Awake()
	{
		Assert.IsNotNull(_backButton);
		Assert.IsNotNull(_ps4Controller);
		Assert.IsNotNull(_pcController);
	}

	private void OnEnable()
	{
		_backButton.onClick.AddListener(BackButton_OnClick);
	}

	private void OnDisable()
	{
		_backButton.onClick.RemoveListener(BackButton_OnClick);
	}

	private void BackButton_OnClick()
	{
		ViewManager.Instance.BackToPreviousView();
	}

	private void Update()
	{
		_ps4Controller.SetActive(InputManager.CurrentInputType == InputType.PS4);
		_pcController.SetActive(InputManager.CurrentInputType == InputType.PC);
	}
}