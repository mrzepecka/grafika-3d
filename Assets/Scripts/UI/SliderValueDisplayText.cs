﻿using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

[RequireComponent(typeof(TextMeshProUGUI))]
public class SliderValueDisplayText : MonoBehaviour
{
    [SerializeField] private Slider _slider;
    private TextMeshProUGUI _text;
    
    private void Awake()
    {
        Assert.IsNotNull(_slider);
        _text = GetComponent<TextMeshProUGUI>();
        Assert.IsNotNull(_text);
    }

    private void Start()
    {
        _text.text = (_slider.value * 100f).ToString("0");
    }

    private void OnEnable()
    {
        _slider.onValueChanged.AddListener(Slider_OnValueChanged);
    }

    private void OnDisable()
    {
        _slider.onValueChanged.RemoveListener(Slider_OnValueChanged);
    }

    private void Slider_OnValueChanged(float value)
    {
        _text.text = (value * 100f).ToString("0");
    }
}