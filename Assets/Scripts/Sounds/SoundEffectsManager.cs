﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class SoundEffectsManager : MonoBehaviour
{
	private static SoundEffectsManager _instance;
	private static object _lock = new object();
	public static SoundEffectsManager Instance
	{
		get
		{
			lock (_lock)
			{
				if (_instance == null)
				{
					_instance = (SoundEffectsManager)FindObjectOfType(typeof(SoundEffectsManager));

					if (FindObjectsOfType(typeof(SoundEffectsManager)).Length > 1)
					{
						Debug.LogError("[Singleton] Something went really wrong " +
						               " - there should never be more than 1 singleton!" +
						               " Reopening the scene might fix it.");
						return _instance;
					}

					if (_instance == null)
					{
						var manager = Resources.Load<SoundEffectsManager>("SoundEffectsManager");
						var singleton = Instantiate(manager);
						_instance = singleton.GetComponent<SoundEffectsManager>();
						
						DontDestroyOnLoad(singleton);

						Debug.Log("[Singleton] An instance of " + typeof(SoundEffectsManager) +
						          " is needed in the scene, so '" + singleton +
						          "' was created with DontDestroyOnLoad.");
					}
					else
					{
						Debug.Log("[Singleton] Using instance already created: " +
						          _instance.gameObject.name);
					}
				}

				return _instance;
			}
		}
	}
	
	[SerializeField] private AudioClip[] _playerPlayerCollision;
	[SerializeField] private AudioClip[] _playerMetalCollision;
	[SerializeField] private AudioClip[] _playerConcreteCollision;
	[SerializeField] private AudioClip[] _playerClothCollision;
	[SerializeField] private AudioClip[] _playerWoodCollision;
	
	[SerializeField] private AudioClip[] _obstacleMetal;
	[SerializeField] private AudioClip[] _obstacleConcrete;
	[SerializeField] private AudioClip[] _obstacleWood;
	
	[SerializeField] private AudioClip[] _concreteConcreteCollision;

	private void PlayEffect(AudioClip audioClip, Vector3 position)
	{
		StartCoroutine(PlayEffectCoroutine(audioClip, position));
	}

	private IEnumerator PlayEffectCoroutine(AudioClip audioClip, Vector3 position)
	{
		var go = new GameObject("SoundEffect-" + audioClip.name);
		go.transform.position = position;
		var audioSource = go.AddComponent<AudioSource>();
		audioSource.volume = GlobalSettings.SoundEffectsVolume;
		audioSource.PlayOneShot(audioClip);

		yield return new WaitForSeconds(audioClip.length);
		Destroy(go);
	}
	
	public void PlayEffect_PlayerCollision(string collisionTag, Vector3 position)
	{
		switch (collisionTag)
		{
			case Constants.Tags.WOOD:
				PlayEffect(_playerWoodCollision[Random.Range(0, _playerWoodCollision.Length)], position);
				break;
			case Constants.Tags.CLOTH:
				PlayEffect(_playerClothCollision[Random.Range(0, _playerClothCollision.Length)], position);
				break;
			case Constants.Tags.METAL:
				PlayEffect(_playerMetalCollision[Random.Range(0, _playerMetalCollision.Length)], position);
				break;
			case Constants.Tags.CONCRETE:
				PlayEffect(_playerConcreteCollision[Random.Range(0, _playerConcreteCollision.Length)], position);
				break;
			case Constants.Tags.PLAYER:
				PlayEffect(_playerPlayerCollision[Random.Range(0, _playerPlayerCollision.Length)], position);
				break;
		}
	}

	public void PlayEffect_ObstacleGroundCollision(string collisionTag, Vector3 position)
	{
		switch (collisionTag)
		{
			case Constants.Tags.WOOD:
				PlayEffect(_obstacleWood[Random.Range(0, _obstacleWood.Length)], position);
				break;
			case Constants.Tags.METAL:
				PlayEffect(_obstacleMetal[Random.Range(0, _obstacleMetal.Length)], position);
				break;
			case Constants.Tags.CONCRETE:
				PlayEffect(_obstacleConcrete[Random.Range(0, _obstacleConcrete.Length)], position);
				break;
		}
	}

	public void PlayEffect_ConcreteConcreteCollision(Vector3 position)
	{
		PlayEffect(_concreteConcreteCollision[Random.Range(0, _concreteConcreteCollision.Length)], position);
	}
}