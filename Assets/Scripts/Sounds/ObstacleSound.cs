﻿using System.Collections;
using System.Runtime.Remoting.Messaging;
using UnityEngine;

public class ObstacleSound : MonoBehaviour
{
    private bool _ready = false;
    [SerializeField] private bool _groundCollisionSound = true;
    
    private IEnumerator Start()
    {
        yield return new WaitForSeconds(3f);
        _ready = true;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (!_ready)
        {
            return;
        }

        var collisionPoint = other.contacts[0].point;
        
        if (other.gameObject.CompareTag(Constants.Tags.GROUND))
        {
            if (_groundCollisionSound)
            {
                SoundEffectsManager.Instance.PlayEffect_ObstacleGroundCollision(gameObject.tag, collisionPoint);
            }
        }

        if (other.gameObject.CompareTag(Constants.Tags.CONCRETE)
            && gameObject.CompareTag(Constants.Tags.CONCRETE))
        {
            SoundEffectsManager.Instance.PlayEffect_ConcreteConcreteCollision(collisionPoint);
        }
    }
}