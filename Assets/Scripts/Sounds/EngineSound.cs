﻿using UnityEngine;
using UnityEngine.Assertions;

[RequireComponent(typeof(AudioSource), typeof(MiniSumoController))]
public class EngineSound : MonoBehaviour
{
	private AudioSource _audioSource;
	private MiniSumoController _miniSumoController;

	private void Awake()
	{
		_audioSource = GetComponent<AudioSource>();
		Assert.IsNotNull(_audioSource);
		
		_miniSumoController = GetComponent<MiniSumoController>();
		Assert.IsNotNull(_miniSumoController);
	}

	private void Start()
	{
		_audioSource.loop = true;
		_audioSource.Play();
	}

	private void Update()
	{
		var newPitch = Mathf.Clamp(_miniSumoController.CurrentSpeed / _miniSumoController.TopSpeed * 8f, 0f, 1f);
		_audioSource.pitch = newPitch;
		_audioSource.volume = GlobalSettings.SoundEffectsVolume * 0.66f;
	}
}